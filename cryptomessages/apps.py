from django.apps import AppConfig


class CryptomessagesConfig(AppConfig):
    name = 'cryptomessages'
