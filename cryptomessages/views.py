from django.views.generic import TemplateView
from.forms import NameForm
from django.views.generic.edit import FormView
from django.http import JsonResponse
from elgamal.elgamal import ElGamalServer


class IndexView(TemplateView):
    template_name = 'cryptomessages/index.html'
    form_class = NameForm
    success_url = '/'

    def get_context_data(self, **kwargs):
        import json
        context = super(IndexView, self).get_context_data(**kwargs)
        elgamal_server = ElGamalServer(256)
        public_keys = elgamal_server.get_public_keys_dict()
        # convert keys to strings since JS does not handle big numbers
        public_keys = {k: str(v) for k, v in public_keys.items()}
        context['public_keys'] = public_keys
        # Save public/private keys in the session so incoming message can be
        # decrypted later
        self.request.session['keys'] = elgamal_server.get_keys_dict()
        return context


def user_encrypted_message(request):
    from block_cipher.aes import Aes
    from block_cipher.operation_mode import CipherBlockChaining
    import json

    encrypted_message = json.loads(request.GET.get('encrypted_message', None))
    encrypted_key = json.loads(request.GET.get('encrypted_key', None))
    encrypted_iv = json.loads(request.GET.get('encrypted_iv', None))

    elgamal_server = ElGamalServer(128, keys_dict=request.session.get('keys'))

    decrypted_key = elgamal_server.decrypt(int(encrypted_key['c1']), int(encrypted_key['c2']))
    print("decrypted_key", decrypted_key)
    decrypted_key = bytearray.fromhex('{:0{}x}'.format(decrypted_key, 64))

    decrypted_iv = elgamal_server.decrypt(int(encrypted_iv['c1']),
                                           int(encrypted_iv['c2']))
    print("decrypted_iv", decrypted_iv)
    decrypted_iv = bytearray.fromhex('{:0{}x}'.format(decrypted_iv, 32))

    aes = Aes(32, decrypted_key)
    cbc = CipherBlockChaining(aes, decrypted_iv)
    decrypted_message = cbc.decrypt(bytearray(encrypted_message))


    data = {
        'decrypted_message': decrypted_message,
        'public_keys': elgamal_server.get_public_keys_dict(),
    }
    return JsonResponse(data)
