$( document ).ready(function() {
    var key = getRangeRand(publicKeys.p, 32);
    var iv = getRangeRand(publicKeys.p, 16);
    console.log("iv ", bigInt.fromArray(iv, 256, false).toString());
    aes = new AES(32, key, iv);
    $("#server-encrypted-key").html(innerBoxKeyHtml(publicKeys));
    var bigKey = bigInt.fromArray(key, 256, false).toString(16)
    $("#client-key").text(bigKey);
    var bigIv = bigInt.fromArray(iv, 256, false).toString(16)
    $("#client-iv").text(bigIv);
    console.log( "ready!" );
});

function getRangeRand(bigMax, numOfBytes) {
    var randBytes = AES.getRandBytes(numOfBytes);
    var bigNum =  bigInt.fromArray(randBytes, 256, false);
    while (bigNum.compare(bigMax) >= 0) {
        randBytes = AES.getRandBytes(numOfBytes);
        bigNum =  bigInt.fromArray(randBytes, 256, false);
    }
    return randBytes;
 }

function encrypt() {
    var str = $("#input-text-area").val();
    var byteArr = stringToAsciiByteArray(str);
    var encryptedBytes = aes.cbcEncryption(byteArr);
    var encryptedMsg = String.fromCharCode.apply(null, encryptedBytes);
    $('#encrypted-text-area').val(encryptedMsg);
}

function elgamalEncrytion(p, g, h, m) {
    var diff = bigInt(m).compare(p);
    if ( diff >= 0 ) {
        console.log('M value should be less than the public key')
    }
    var k = bigInt.randBetween(1, p - 2);
    var c1 = bigInt(g).modPow(k, p);
    var pw = bigInt(h).modPow(k, p);
    var c2 = bigInt(m).multiply(pw).mod(p);
    return {'c1': c1.toJSON(), 'c2':c2.toJSON()};
}

function sendToServer() {
    var str = $("#input-text-area").val();
    // Locally encrypt
    var byteArr = stringToAsciiByteArray(str);
    console.log("msg in bytes", byteArr)
    var encryptedBytes = aes.cbcEncryption(byteArr);

    // Encrypt key and iv and if using elgamal's public keys
    var bigKey = bigInt.fromArray(aes.key, 256, false)
    var key_dict = elgamalEncrytion(publicKeys.p, publicKeys.g, publicKeys.h, bigKey)
    var bigIv = bigInt.fromArray(aes.iv, 256, false)
    var iv_dict = elgamalEncrytion(publicKeys.p, publicKeys.g, publicKeys.h, bigIv)
    var encryptedKey = JSON.stringify(key_dict);
    var encryptedIv = JSON.stringify(iv_dict);

    // Populate information in public space
    $("#local-encrypted-key").html(innerBoxKeyHtml(key_dict));
    $("#local-encrypted-iv").html(innerBoxKeyHtml(iv_dict));
    var encryptedMsg = String.fromCharCode.apply(null, encryptedBytes);
    $("#local-encrypted-msg").text(encryptedMsg);
    console.log("key", bigKey.toString());
    console.log("iv", bigIv.toString());
    // Send to server
    $.ajax({
        url: '/ajax/user_encrypted_message/',
        data: {
            'encrypted_message': JSON.stringify(encryptedBytes),
            'encrypted_key': encryptedKey,
            'encrypted_iv': encryptedIv,
        },
        dataType: 'json',
        success: function (data) {
            if (data.decrypted_message) {
                $("#server-decrypted-text-area").text(data.decrypted_message);
            }
        },
        error: function(){
            alert('Decryption failed');
        }
    });
}

function innerBoxKeyHtml(dict) {
    var str = "";
    for (var key in dict){
        str += "<strong>" + key + ":</strong> "
        + bigInt(dict[key]).toString(16)
        + "<br />"
    }
    return str
}