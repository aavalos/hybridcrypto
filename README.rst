hybridcrypto
============

Implements AES and ElGamal encryption


Requirements
------------

1. Make sure to have have installed Python 3.6. 
To ensure you have the correct version run: python --version


Installation
------------

1. Download the zip/tar file or clone directly from the repo into the desired folder location:
2. Install requirements by running the following
    
    $ pip install -r requirements.txt

Setup
------
1. Setup a local server  

    $ python manage.py makemigrations
    
    $ python manage.py migrate
    
2. Run the local server  

    $ python manage.py runserver

3. Check local website at:

    http://127.0.0.1:8000/
    
4. Enjoy :)
